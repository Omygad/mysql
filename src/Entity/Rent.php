<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RentRepository")
 */
class Rent
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $rentStartDay;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $rentEndDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="rents")
     * @ORM\JoinColumn(nullable=false)
     */
    private $client;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Computer", inversedBy="rents")
     * @ORM\JoinColumn(nullable=false)
     */
    private $computer;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRentStartDay(): ?\DateTimeInterface
    {
        return $this->rentStartDay;
    }

    public function setRentStartDay(\DateTimeInterface $rentStartDay): self
    {
        $this->rentStartDay = $rentStartDay;

        return $this;
    }

    public function getRentEndDate(): ?string
    {
        return $this->rentEndDate;
    }

    public function setRentEndDate(string $rentEndDate): self
    {
        $this->rentEndDate = $rentEndDate;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getComputer(): ?Computer
    {
        return $this->computer;
    }

    public function setComputer(?Computer $computer): self
    {
        $this->computer = $computer;

        return $this;
    }
}
