<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190221095713 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE rent ADD client_id INT NOT NULL, ADD computer_id INT NOT NULL');
        $this->addSql('ALTER TABLE rent ADD CONSTRAINT FK_2784DCC19EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE rent ADD CONSTRAINT FK_2784DCCA426D518 FOREIGN KEY (computer_id) REFERENCES computer (id)');
        $this->addSql('CREATE INDEX IDX_2784DCC19EB6921 ON rent (client_id)');
        $this->addSql('CREATE INDEX IDX_2784DCCA426D518 ON rent (computer_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE rent DROP FOREIGN KEY FK_2784DCC19EB6921');
        $this->addSql('ALTER TABLE rent DROP FOREIGN KEY FK_2784DCCA426D518');
        $this->addSql('DROP INDEX IDX_2784DCC19EB6921 ON rent');
        $this->addSql('DROP INDEX IDX_2784DCCA426D518 ON rent');
        $this->addSql('ALTER TABLE rent DROP client_id, DROP computer_id');
    }
}
